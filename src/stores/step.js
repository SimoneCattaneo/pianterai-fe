import { defineStore } from 'pinia'

export const useStepStore = defineStore('step', {
  state: () => {
    return {
      currentStep: 'startup',
      isSetup: true
    }
  },
  actions: {
    updateSetupStatus(val){
      this.isSetup = val
    }
  }
})