# PianterAI

Welcome to the unofficial GitLab repository for the PianterAI project! This repository houses the source code and related files for an app developed during a hackathon. PianterAI is an app developed to assist house farmers and gardening enthusiasts in growing plants easily. Please note that this repository is not an official repository for the project.

## Project Overview

PianterAI is a mobile application aimed at simplifying the process of plant cultivation. It provides users with valuable information, tips, and reminders to help them grow and nurture their plants effectively. The app caters to both seasoned gardeners and beginners, offering a user-friendly interface and a range of features to enhance the gardening experience.

## Contents

This repository contains the following key components:

1. **Source Code**: The source code for the PianterAI mobile app can be found in the `src` directory. It includes all the necessary files and directories required to build and run the application.

2. **Documentation**: The `docs` directory contains any relevant documentation for the project. This may include user guides, developer documentation, or API references, depending on the stage of development.

3. **Assets**: The `assets` directory holds any additional assets used by the app, such as images, icons, or multimedia files.


## Usage

To utilize PianterAI, follow the instructions below:

1. Clone or download the repository to your local machine.
   **shell** `git clone git@gitlab.com:SimoneCattaneo/pianterai-fe.git`

2. Run `npm install`

3. Run `npm run dev`


## License

The code and files in this repository are licensed under the [GNU General Public License v3.0](LICENSE). Please review the license file for more details.
